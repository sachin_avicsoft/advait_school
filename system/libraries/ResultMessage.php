<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class CI_ResultMessage {
        
    public $SUCCESS = 'success';
    public $ERROR = 'error';     
        
    public $message;    
    public $messageType;
   
     public function __construct(){
        //parent::__construct();
    }

    function initialize($message, $messageType){        
        $this->message = $message;
        $this->messageType = $messageType;
    }
    
    function printResultMessage($message, $messageType){
        
        $this->initialize($message, $messageType);
        
        if(null != $this->message){    
            if($this->messageType == $this->SUCCESS){
                return "<div class='alert alert-success'><i class='fa fa-check-circle'></i>&nbsp;".$this->message."<a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times'></i></a></div>";
            } else {
                return "<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i>&nbsp;".$this->message."<a href='#' class='close' data-dismiss='alert' aria-label='close'><i class='fa fa-times'></i></a></div>";
            }
        }        
    }

}

?>