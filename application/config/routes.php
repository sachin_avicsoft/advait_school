<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['about'] = 'welcome/about';
$route['teachers'] = 'welcome/teachers';
$route['gallery'] = 'welcome/gallery';
$route['contact'] = 'welcome/contact';
$route['contact/send'] = 'welcome/send';
$route['single'] = 'welcome/single';

//admin
$route['login'] ='LoginController';
$route['logout'] ='LoginController/logout';

$route['admin/dashboard'] = 'DashboardController';
$route['admin/dashboard/getdata'] = 'DashboardController/getdata';

//teacher
$route['admin/teachers'] = 'AdminTeacherController';
$route['admin/teachers/add'] = 'AdminTeacherController/add';
$route['admin/teachers/edit'] = 'AdminTeacherController/edit';
$route['admin/teachers/delete'] = 'AdminTeacherController/delete';
$route['admin/teachers/save'] = 'AdminTeacherController/save';
$route['admin/teachers/list'] = 'AdminTeacherController/teacherList';

//student
$route['admin/students'] = 'AdminStudentController';
$route['admin/students/add'] = 'AdminStudentController/add';
$route['admin/students/edit'] = 'AdminStudentController/edit';
$route['admin/students/delete'] = 'AdminStudentController/delete';
$route['admin/students/save'] = 'AdminStudentController/save';
$route['admin/students/list'] = 'AdminStudentController/studentList';

//standard
$route['admin/standards'] = 'AdminStandardController';
$route['admin/standards/add'] = 'AdminStandardController/add';
$route['admin/standards/edit'] = 'AdminStandardController/edit';
$route['admin/standards/delete'] = 'AdminStandardController/delete';
$route['admin/standards/save'] = 'AdminStandardController/save';
$route['admin/standards/list'] = 'AdminStandardController/StandardList';

//batch
$route['admin/batches'] = 'AdminBatchController';
$route['admin/batches/add'] = 'AdminBatchController/add';
$route['admin/batches/edit'] = 'AdminBatchController/edit';
$route['admin/batches/delete'] = 'AdminBatchController/delete';
$route['admin/batches/save'] = 'AdminBatchController/save';
$route['admin/batches/list'] = 'AdminBatchController/batchList';
$route['admin/batches/getbatches'] = 'AdminBatchController/getBatchListByStandard';
$route['admin/batches/print'] = 'AdminBatchController/printPage';

//batch
$route['admin/batchyears'] = 'AdminBatchYearController';
$route['admin/batchyears/add'] = 'AdminBatchYearController/add';
$route['admin/batchyears/edit'] = 'AdminBatchYearController/edit';
$route['admin/batchyears/delete'] = 'AdminBatchYearController/delete';
$route['admin/batchyears/save'] = 'AdminBatchYearController/save';
$route['admin/batchyears/list'] = 'AdminBatchYearController/batchYearList';

//settings
$route['admin/settings'] = 'AdminSettingController';
$route['admin/settings/save'] = 'AdminSettingController/save';

$route['404_override'] = 'welcome/error';
$route['translate_uri_dashes'] = FALSE;