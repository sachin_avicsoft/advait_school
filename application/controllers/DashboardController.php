<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();        
        
        $this->checkLogin();
        //$this->checkRoles(array("ADMIN"));
        $this->load->model('User');
        $this->load->model('Batch');
        $this->load->model('BatchYear');
        $this->load->model('Student');
        $this->load->model('Standard');
        $this->load->model('Teacher');
     }
    
    public function index(){
        $batchList = $this->Batch->get_batch_list();
        if($batchList){
            $batchCount = count($batchList);
        } else {
            $batchCount = 0;
        }
        $standardList = $this->Standard->get_standard_list();
        if($standardList){
            $standardCount = count($standardList);
        } else {
            $standardCount = 0;
        }
        $studentList = $this->Student->get_student_list();
        if($studentList){
            $studentCount = count($studentList);
        } else {
            $studentCount = 0;
        }
        $teacherList = $this->Teacher->get_teacher_list();
        if($teacherList){
            $teacherCount = count($teacherList);
        } else {
            $teacherCount = 0;
        }
        $data = array(
            'batch_count' => $batchCount,
            'teacher_count' => $teacherCount,
            'standard_count' => $standardCount,
            'student_count' => $studentCount,
        );
        $data['batch_year_list'] = $this->BatchYear->get_batch_year_list();
        $this->load->view('admin/dashboard/index',$data);
    }
    
    public function getdata(){ 
        $batch_year_id = $this->input->get('batch_year');
        $batch = $this->Batch->get_all_batch_student_report_by_batch_year($batch_year_id); 
        if($batch){
            
            for($i = 0; $i < count($batch); $i++){
                $male_count = $this->Student->get_student_count_by_batch($batch[$i]['id'],'Male');
                $female_count = $this->Student->get_student_count_by_batch($batch[$i]['id'],'Female');
                $batch[$i]['boys'] = $male_count;
                $batch[$i]['girls'] = $female_count;
            }
            
            $data = $batch;
            
            $responce->cols[] = array( 
            "id" => "", 
            "label" => "Batches", 
            "pattern" => "", 
            "type" => "string" 
            ); 
            $responce->cols[] = array( 
                "id" => "", 
                "label" => "Boys", 
                "pattern" => "", 
                "type" => "number" 
            ); 
            $responce->cols[] = array( 
                "id" => "", 
                "label" => "Girls", 
                "pattern" => "", 
                "type" => "number" 
            );
            
            foreach($data as $key => $value) 
            { 
                $responce->rows[]["c"] = array( 
                    array( 
                        "v" => $value['standard_name'].'-'.$value['batch_name'], 
                        "f" => null 
                    ) , 
                    array( 
                        "v" => (int)$value['boys'][0]['ncount'], 
                        "f" => null 
                    ) , 
                    array( 
                        "v" => (int)$value['girls'][0]['ncount'], 
                        "f" => null 
                    ) 
                ); 
            } 
 
            echo json_encode($responce); 
        } else {
            $responce = '';
            echo json_encode($responce); 
        }
    }
    
}