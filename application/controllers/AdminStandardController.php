<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminStandardController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();        
        
        $this->checkLogin();
        $this->load->model('User');
        $this->load->model('Standard');
     }
    
    public function index(){
        $this->load->view('admin/standard/index');
    }
    
    public function add(){
        $data = array(
            'id' => '',
            'standard_name' => '',
        );
        $this->load->view('admin/standard/form',$data);
    }
    
    public function edit(){
        $id = $this->input->get('id');
        $row = $this->Standard->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'standard_name' => $row['standard_name'],
            );
            $this->load->view('admin/standard/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Id is not present', "error"));
            redirect('admin/standards');
        }
    }
    
    public function save(){
        $this->form_validation->set_rules('standard_name', 'standard_name', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->resultmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('admin/standards/add');
            } else {
                redirect('admin/standards/edit?id='.$_POST['id']);
            }
        } else { 
            
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $standardId = $this->Standard->add();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Standard added successfully', "success"));
            } else {
                $standardId = $_POST['id'];
                $this->Standard->update();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Standard updated successfully', "success"));
            }
            redirect('admin/standards');
        }
    }
    
    public function delete(){
        $id = $this->input->get('id');
        $ajaxResult = array();
        if($id == ""){
            $ajaxResult['msg'] = "Id is not present.";
        } else {
            $this->Standard->delete();
            $ajaxResult['result'] = "success";
        }
        echo utf8_encode(json_encode($ajaxResult));
        exit;
    }
    
    public function standardList(){
        $start = $this->input->get_post('start', true);
        $limit = $this->input->get_post('length', true); 
        $data = $this->Standard->standardList($limit, $start);
        echo json_encode($data); 
        exit;
    }
    
}