<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminTeacherController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();        
        
        $this->checkLogin();
        $this->load->model('User');
        $this->load->model('Teacher');
     }
    
    public function index(){
        $this->load->view('admin/teacher/index');
    }
    
    public function add(){
        $data = array(
            'id' => '',
            'teacher_name' => '',
            'teacher_address' => '',
            'teacher_contact' => '',
            'teacher_gender' => '',
            'teacher_birth_date' => '',
            'teacher_email' => '',
        );
        $this->load->view('admin/teacher/form',$data);
    }
    
    public function edit(){
        $id = $this->input->get('id');
        $row = $this->Teacher->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'teacher_name' => $row['teacher_name'],
                'teacher_address' => $row['teacher_address'],
                'teacher_contact' => $row['teacher_contact'],
                'teacher_gender' => $row['teacher_gender'],
                'teacher_birth_date' => $row['teacher_birth_date'],
                'teacher_email' => $row['teacher_email'],
            );
            $this->load->view('admin/teacher/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Id is not present', "error"));
            redirect('admin/teachers');
        }
    }
    
    public function save(){
        $this->form_validation->set_rules('teacher_name', 'teacher_name', 'required');
        $this->form_validation->set_rules('teacher_contact', 'teacher_contact', 'required');
        $this->form_validation->set_rules('teacher_address', 'teacher_address', 'required');
        $this->form_validation->set_rules('teacher_email', 'teacher_email', 'required');
        $this->form_validation->set_rules('teacher_gender', 'teacher_gender', 'required');
        $this->form_validation->set_rules('teacher_birth_date', 'teacher_birth_date', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->resultmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('admin/teachers/add');
            } else {
                redirect('admin/teachers/edit?id='.$_POST['id']);
            }
        } else { 
            
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $teacherId = $this->Teacher->add();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Teacher added successfully', "success"));
            } else {
                $teacherId = $_POST['id'];
                $this->Teacher->update();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Teacher updated successfully', "success"));
            }
            redirect('admin/teachers');
        }
    }
    
    public function delete(){
        $id = $this->input->get('id');
        $ajaxResult = array();
        if($id == ""){
            $ajaxResult['msg'] = "Id is not present.";
        } else {
            $this->Teacher->delete();
            $ajaxResult['result'] = "success";
        }
        echo utf8_encode(json_encode($ajaxResult));
        exit;
    }
    
    public function teacherList(){
        $start = $this->input->get_post('start', true);
        $limit = $this->input->get_post('length', true); 
        $data = $this->Teacher->teacherList($limit, $start);
        echo json_encode($data); 
        exit;
    }
    
}