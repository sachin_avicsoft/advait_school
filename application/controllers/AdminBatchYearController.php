<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBatchYearController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();        
        
        $this->checkLogin();
        $this->load->model('BatchYear');
     }
    
    public function index(){
        $this->load->view('admin/batchyear/index');
    }
    
    public function add(){
        $data = array(
            'id' => '',
            'batch_year' => '',
        );
        $this->load->view('admin/batchyear/form',$data);
    }
    
    public function edit(){
        $id = $this->input->get('id');
        $row = $this->BatchYear->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'batch_year' => $row['batch_year'],
            );
            $this->load->view('admin/batchyear/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Id is not present', "error"));
            redirect('admin/batchyears');
        }
    }
    
    public function save(){
        $this->form_validation->set_rules('batch_year', 'batch_year', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->resultmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('admin/batchyears/add');
            } else {
                redirect('admin/batchyears/edit?id='.$_POST['id']);
            }
        } else { 
            
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $batchYearId = $this->BatchYear->add();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Batch Year added successfully', "success"));
            } else {
                $batchYearId = $_POST['id'];
                $this->BatchYear->update();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Batch Year updated successfully', "success"));
            }
            redirect('admin/batchyears');
        }
    }
    
    public function delete(){
        $id = $this->input->get('id');
        $ajaxResult = array();
        if($id == ""){
            $ajaxResult['msg'] = "Id is not present.";
        } else {
            $this->BatchYear->delete();
            $ajaxResult['result'] = "success";
        }
        echo utf8_encode(json_encode($ajaxResult));
        exit;
    }
    
    public function batchYearList(){
        $start = $this->input->get_post('start', true);
        $limit = $this->input->get_post('length', true); 
        $data = $this->BatchYear->batchYearList($limit, $start);
        echo json_encode($data); 
        exit;
    }
    
}