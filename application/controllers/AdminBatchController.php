<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBatchController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();        
        
        $this->checkLogin();
        $this->load->model('Teacher');
        $this->load->model('Standard');
        $this->load->model('Batch');
        $this->load->model('Setting');
        $this->load->model('Student');
        $this->load->model('BatchYear');
     }
    
    public function index(){
        $this->load->view('admin/batch/index');
    }
    
    public function add(){
        $data = array(
            'id' => '',
            'teacher_id' => '',
            'standard_id' => '',
            'batch_name' => '',
            'batch_year_id' => '',
        );
        $data['teacher_list'] = $this->Teacher->get_teacher_list();
        $data['batch_year_list'] = $this->BatchYear->get_batch_year_list();
        $data['standard_list'] = $this->Standard->get_standard_list();
        $this->load->view('admin/batch/form',$data);
    }
    
    public function edit(){
        $id = $this->input->get('id');
        $row = $this->Batch->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'teacher_id' => $row['teacher_id'],
                'standard_id' => $row['standard_id'],
                'batch_name' => $row['batch_name'],
                'batch_year_id' => $row['batch_year_id'],
            );
            $data['teacher_list'] = $this->Teacher->get_teacher_list();
            $data['batch_year_list'] = $this->BatchYear->get_batch_year_list();
            $data['standard_list'] = $this->Standard->get_standard_list();
            $this->load->view('admin/batch/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Id is not present', "error"));
            redirect('admin/batches');
        }
    }
    
    public function printPage(){
        $id = $this->input->get('id');
        $row = $this->Batch->get($id);
        $setting = $this->Setting->get_data();
        if($row){
            $data = array(
                'id' => $row['id'],
                'teacher_id' => $row['teacher_id'],
                'standard_id' => $row['standard_id'],
                'batch_name' => $row['batch_name'],
                'batch_year_id' => $row['batch_year_id'],
            );
            $batchYear = $this->BatchYear->get($row['batch_year_id']);
            $data['batch_year'] = $batchYear['batch_year'];
            $standard = $this->Standard->get($row['standard_id']);
            $data['standard_name'] = $standard['standard_name'];
            $teacher = $this->Teacher->get($row['teacher_id']);
            $data['teacher_name'] = $teacher['teacher_name'];
            $data['teacher_contact'] = $teacher['teacher_contact'];
            $data['setting'] = $setting;
            $student_list = $this->Student->get_student_list_by_batch($id);
            $data['student_list'] = $student_list;
            $this->load->view('admin/batch/print',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('This page is not available', "error"));
            redirect('admin/batches');
        }
    }
    
    public function save(){
        $this->form_validation->set_rules('teacher_id', 'teacher_id', 'required');
        $this->form_validation->set_rules('standard_id', 'standard_id', 'required');
        $this->form_validation->set_rules('batch_name', 'batch_name', 'required');
        $this->form_validation->set_rules('batch_year_id', 'batch_year_id', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->resultmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('admin/batches/add');
            } else {
                redirect('admin/batches/edit?id='.$_POST['id']);
            }
        } else { 
            
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $batchId = $this->Batch->add();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Batch added successfully', "success"));
            } else {
                $batchId = $_POST['id'];
                $this->Batch->update();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Batch updated successfully', "success"));
            }
            redirect('admin/batches');
        }
    }
    
    public function delete(){
        $id = $this->input->get('id');
        $ajaxResult = array();
        if($id == ""){
            $ajaxResult['msg'] = "Id is not present.";
        } else {
            $this->Batch->delete();
            $ajaxResult['result'] = "success";
        }
        echo utf8_encode(json_encode($ajaxResult));
        exit;
    }
    
    public function batchList(){
        $start = $this->input->get_post('start', true);
        $limit = $this->input->get_post('length', true); 
        $data = $this->Batch->batchList($limit, $start);
        echo json_encode($data); 
        exit;
    }
    
    public function getBatchListByStandard(){
        $standard_id = $this->input->get('standard_id');
        $data = $this->Batch->get_batch_list_by_standard_id($standard_id);
        echo json_encode($data); 
        exit;
    }
    
}