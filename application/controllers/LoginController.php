<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends MY_Controller {

    public function __construct() {
        parent::__construct();   
        
        $this->load->model('User');
    }

    public function index(){
	    
        if(isset($_POST['action']) && $_POST['action'] == "login"){
					
            $username = $this->input->post("username");
            $password = $this->input->post("password");

            $this->form_validation->set_rules("username", "username", "trim|required");
            $this->form_validation->set_rules("password", "password", "trim|required");

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">All fields are required!</div>');
                    redirect('login');
            } else {
			
                $usr_result = $this->User->get_entry($username, $password);	
                if($usr_result) {
                    //set the session variables                 
                    $sessiondata = array(
                        'user_id' => $usr_result['id'],
                        'username' => $usr_result['username'],
                        'loginuser' => TRUE
                    );
                    $this->session->set_userdata('logged_in', $sessiondata);
                    redirect('admin/dashboard');
                    
                } else {
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
                    redirect('login');				 
                }			
            }
        } else {		
            $this->load->view('login');
        }
    }
    
    public function logout(){
        $this->session->unset_userdata('logged_in');       
        redirect('/', 'refresh');		
    }
}