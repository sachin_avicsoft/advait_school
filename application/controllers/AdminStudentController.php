<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminStudentController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();        
        
        $this->checkLogin();
        $this->load->model('Student');
        $this->load->model('Standard');
        $this->load->model('Batch');
     }
    
    public function index(){
        $this->load->view('admin/student/index');
    }
    
    public function add(){
        $data = array(
            'id' => '',
            'student_name' => '',
            'student_address' => '',
            'student_contact' => '',
            'student_gender' => '',
            'student_email' => '',
            'student_birth_date' => '',
            'batch_id' => '',
        );
        $data['standard_list'] = $this->Standard->get_standard_list();
        $data['standard_id'] = $data['standard_list'][0]['id'];
        $data['batch_list'] = $this->Batch->get_batch_list_by_standard_id($data['standard_list'][0]['id']);
        
        $this->load->view('admin/student/form',$data);
    }
    
    public function edit(){
        $id = $this->input->get('id');
        $row = $this->Student->get($id);
        if($row){
            $data = array(
                'id' => $row['id'],
                'student_name' => $row['student_name'],
                'student_address' => $row['student_address'],
                'student_contact' => $row['student_contact'],
                'student_gender' => $row['student_gender'],
                'student_email' => $row['student_email'],
                'student_birth_date' => $row['student_birth_date'],
                'batch_id' => $row['batch_id'],
            );
            $batch = $this->Batch->get($row['batch_id']);
            $data['standard_id'] = $batch['standard_id'];
            $data['batch_list'] = $this->Batch->get_batch_list_by_standard_id($batch['standard_id']);
            $data['standard_list'] = $this->Standard->get_standard_list();
            $this->load->view('admin/student/form',$data);
        } else {
            $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Id is not present', "error"));
            redirect('admin/students');
        }
    }
    
    public function save(){
        $this->form_validation->set_rules('student_name', 'student_name', 'required');
        $this->form_validation->set_rules('student_contact', 'student_contact', 'required');
        $this->form_validation->set_rules('student_address', 'student_address', 'required');
        $this->form_validation->set_rules('student_birth_date', 'student_birth_date', 'required');
        $this->form_validation->set_rules('student_gender', 'student_gender', 'required');
        $this->form_validation->set_rules('batch_id', 'batch_id', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->resultmessage->printResultMessage('All fields are required', "error"));
            if(isset($_POST['id']) && $_POST['id'] == ""){  
                redirect('admin/students/add');
            } else {
                redirect('admin/students/edit?id='.$_POST['id']);
            }
        } else { 
            
            if(isset($_POST['id']) && $_POST['id'] == ""){                                                                     
                $studentId = $this->Student->add();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Student added successfully', "success"));
            } else {
                $studentId = $_POST['id'];
                $this->Student->update();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('Student updated successfully', "success"));
            }
            redirect('admin/students');
        }
    }
    
    public function delete(){
        $id = $this->input->get('id');
        $ajaxResult = array();
        if($id == ""){
            $ajaxResult['msg'] = "Id is not present.";
        } else {
            $this->Student->delete();
            $ajaxResult['result'] = "success";
        }
        echo utf8_encode(json_encode($ajaxResult));
        exit;
    }
    
    public function studentList(){
        $start = $this->input->get_post('start', true);
        $limit = $this->input->get_post('length', true); 
        $search = $this->input->get_post('inputSearch', true);
        $data = $this->Student->studentList($limit, $start, $search);
        echo json_encode($data); 
        exit;
    }
    
}