<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
    
	public function index()
	{
		$this->load->view('home');
	}
        
        public function about()
	{
		$this->load->view('about');
	}
        
        public function teachers()
	{
		$this->load->view('teachers');
	}
        
        public function gallery()
	{
		$this->load->view('gallery');
	}
        
        public function contact()
	{
		$this->load->view('contact');
	}
        
        public function send(){
            //mail send or store in db
            redirect('contact');
        }
        
        public function error(){
            $this->load->view('404');
        }
        
        public function single()
	{
            redirect('/');
            //$this->load->view('single');
	}
}
