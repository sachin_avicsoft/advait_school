<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSettingController extends MY_Controller {
    
     public function __construct() {
        
        parent::__construct();        
        
        $this->checkLogin();
        $this->load->library('upload');
        $this->load->model('Setting');
     }
    
    public function index(){
        $row = $this->Setting->get_data();
        if($row){
            $data = array(
                'id' => $row['id'] ,
                'school_name' => $row['school_name'],
                'school_address' => $row['school_address'],
                'school_contact' => $row['school_contact'],
            );
            
            if($row['fileName'] != "" || $row['fileName'] != NULL){
                $imageFile = $row;
                $data['imageFile'] = $imageFile;
            }
        } else {
            $data = array(
                'id' => '',
                'school_name' => '',
                'school_address' => '',
                'school_contact' => '',
            );
        }
        
        
        $this->load->view('admin/setting/index',$data);
    }
    
    public function save(){
        $this->form_validation->set_rules('school_name', 'school_name', 'required');
        $this->form_validation->set_rules('school_address', 'school_address', 'required');
        $this->form_validation->set_rules('school_contact', 'school_contact', 'required');
        if($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('msg',
                    $this->resultmessage->printResultMessage('All fields are required', "error"));
                redirect('admin/settings');
        } else { 
            if(isset($_POST['id']) && $_POST['id'] == ""){    
                $settingId = $this->Setting->add();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('School data added successfully', "success"));
            } else {
                $settingId = $_POST['id'];
                $this->Setting->update();
                $this->session->set_flashdata('msg', 
                    $this->resultmessage->printResultMessage('School data updated successfully', "success"));
            }

            //**file upload     
            if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {         
                if(! (count($_FILES['userfile']['name']) == 1 && $_FILES['userfile']['name'][0] == "")) {
                    $files = $_FILES;                
                    $cpt = count($_FILES['userfile']['name']);                
                    for($i = 0; $i < $cpt; $i++) {

                        $originalFileName = $files['userfile']['name'][$i];
                        $ext = substr($originalFileName, strrpos($originalFileName, ".") + 1);                                
                        $imageFileId = $this->Setting->insert_file($originalFileName, $_FILES['userfile']['type'][$i], $ext, $settingId);
                        $_FILES['userfile']['name'] = $settingId.".".$ext;
                        $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                        $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                        $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                        $_FILES['userfile']['size'] = $files['userfile']['size'][$i];    

                        $this->upload->initialize($this->set_upload_options());
                        if($this->upload->do_upload()){
                            $upload_data  = $this->upload->data();
                            $this->Setting->updateLocation($settingId,$upload_data["full_path"]);
                        }   else {
                            //echo $this->upload->display_errors();
                             $this->session->set_flashdata('msg', 
                                $this->resultmessage->printResultMessage($this->upload->display_errors(), "error"));
                        }         
                    }
                }
            }
            redirect('admin/settings');
        }
    }
    
    public function set_upload_options(){   
        $config = array();
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = '*';
        $config['max_size']      = '5000KB';
        $config['overwrite']     = TRUE;
        return $config;
    }
}