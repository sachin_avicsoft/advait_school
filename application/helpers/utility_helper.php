<?php if( !defined( 'BASEPATH' ) ) exit( 'No direct script access allowed' );

function resource_url(){
   return base_url().'resources/';
}

function print_resource_url($path){
   echo base_url().'resources/'.$path;
}


/*
|--------------------------------------------------------------------------
| Static Resource Library
|--------------------------------------------------------------------------
|
| Some simple code to help manage/serve static
| files efficiently.
| Refer : https://github.com/erwaller/static-resource-helper-for-codeigniter
*/

?>