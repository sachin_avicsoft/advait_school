<!DOCTYPE HTML>
<html>
<head>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <?php $this->load->view('include/css.php');?>
</head>
<body>
	<!--start-home-->
	<!----- start-header---->
    <div id="home" class="header two">
        <div class="top-header two">	
            <div class="container">
                <div class="logo">
                    <a href="<?php echo site_url(); ?>"><h1>Advait<span>School</span></h1></a>
                </div>
                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul class="cl-effect-16">
                        <li><a href="<?php echo site_url('/'); ?>" data-hover="Home">Home</a></li>
                        <li><a class="active" href="<?php echo site_url('about'); ?>" data-hover="About">About</a></li>
                        <li><a href="<?php echo site_url('teachers'); ?>" data-hover="Teachers">Teachers</a></li>
                        <li><a href="<?php echo site_url('gallery'); ?>" data-hover="Gallery">Gallery</a></li>
                        <li><a href="<?php echo site_url('contact'); ?>" data-hover="Contact">Contact</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
		<!----- //End-slider---->
<!---start-slide-bottom--->
    <div class="about-section">
        <div class="container">
            <div class="about-head">
                <h3>About Us</h3>
                <p>A Few words about us</p>
            </div>
            <div class="col-md-5 slide-about">
                <div class="side">
                    <div  id="top" class="callbacks_container">
                        <ul class="rslides" id="slider4">
                            <li>
                                <img src="<?php echo print_resource_url('web/images/s1.jpg'); ?>" class="img-responsive" alt="" />
                            </li>
                            <li>
                                <img src="<?php echo print_resource_url('web/images/s2.jpg'); ?>" class="img-responsive" alt="" />
                            </li>
                            <li>
                                <img src="<?php echo print_resource_url('web/images/s1.jpg'); ?>" class="img-responsive" alt="" />
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-7 about-text">
                <p><b>ADVAIT ENGLISH MEDIUM SCHOOL</b> run by Late.Popatrao Manjre bahuddeshiy Sanstha and is <b>Gov. Recognised and Self Financed</b>. We also provide education in English and Semi English Medium (Pre Primary and Primary Section). The school's name, Advaits, which comes from Indian origin, is generally means Unique. And our aim as educators is to provide unique education to groom students in harmony with global progress.</p>
                <p>The Advait School aims to provide a stimulating environment and enriched curriculum, which promotes, curiosity, free thinking, articulating, discussing, debating and executing ideas in an environment of respect for pluralism.The main goal is to make available quality education in different areas of knowledge to the students as per their choices and inclinations.</p>
                <span>Our curriculum envisages integrated learning experiences for children. In order to achieve this, a strong emphasis is placed on planning. Within the framework of the curriculum, there is a flexibility to plan a programme that is appropriate to the individual circumstances and to the needs, aptitudes and interests of the children. We present content, approaches and methodologies in such ways to adapt and interpret the curriculum where necessary to meet learners’ own unique requirements. Learning thought experiences/ opportunities, through play and fun, understanding and application forms the crux of our learning approach.</span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
       <!---start-about-bottom--->
    <div class="slide-bottom">
        <div class="slide-bottom-grids">
            <div class="container">
                <div class="col-md-6 slide-bottom-grid">
                    <h3>Welcome!</h3>
                    <p>Top schools in Pune have always attracted students from all over the country and abroad, and in such an environment The Advait School is a preferred choice of parents seeking admission to good schools in Pune.</p>
                </div>
                <div class="col-md-6 slide-bottom-grid">
                    <h3>Our Mission</h3>
                    <p>The school mission is to encourage the natural curiosity of young minds and inculcate a lifelong habit of   “continuous learning” in every child. Learning, of course, is not to be restricted to the classrooms, but continues outside: on the play field, in gardens and just about everywhere.</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="member-section">
        <div class="container">
            <div class="member-head">
                <h3>Our Members</h3>
                <p>2017 Board Members</p>
            </div>
            <div class="members">
                <div class="col-md-4 member-grids">
                    <a href="#"> <img src="<?php echo print_resource_url('web/images/m2.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <h5>President</h5>
                    <p>Manoj P Manjare. <br /> contact no - 9730095000</p>
                </div>
                <div class="col-md-4 member-grids">
                    <a href="#"> <img src="<?php echo print_resource_url('web/images/m2.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <h5>Vice President</h5>
                    <p>Kanhu N. Domale <br/> contact no - 9850131971</p>
                </div>
                <div class="col-md-4 member-grids">
                    <a href="#"> <img src="<?php echo print_resource_url('web/images/m2.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <h5>Secretary </h5>
                    <p>Gautam L. Waghmare <br/> contact no - 8379832330 </p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<!--/mid-bg-->
    <div class="mid-bg">
        <div class="container">
            <div class="mid-section">
                <h3>First Day at School!</h3>
                <h4>ARE YOU READY ?</h4>
                <p>Welcome to school days.</p>
            </div>
        </div>
    </div>
    <?php $this->load->view('include/footer.php');?>
    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <?php $this->load->view('include/js.php');?>
<script type="text/javascript">
    $("span.menu").click(function(){
        $(".top-menu ul").slideToggle("slow" , function(){
        });
    });
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
    
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
    
    $(document).ready(function() {
        /*
        var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
        };
        */

        //$().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
</body>
</html>