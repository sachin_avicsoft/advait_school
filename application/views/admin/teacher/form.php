<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body class="sidebar-mini skin-red-light">
    <div class="wrapper">
        <?php $this->load->view('admin/include/header', 'refresh'); ?> 
        <?php $this->load->view('admin/include/sidebar', 'refresh'); ?> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 923px;">
          <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Teachers
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Teachers</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <?php
                                        if( $id != ""){
                                            echo 'Edit Teacher';
                                        } else {
                                            echo 'Add Teacher';
                                        }
                                    ?>
                                </h3>
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            </div>
                            <div class="box-body">
                                <form id="teacherForm" class="form form-horizontal" method="post" action="<?php echo site_url('admin/teachers/save'); ?>" enctype='multipart/form-data'>
                                    
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Teacher Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="teacher_name" value="<?php echo $teacher_name; ?>" id="teacher_name" class="form-control" placeholder="Enter teacher name" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Contact No</label>
                                        <div class="col-md-8">
                                            <input type="text" name="teacher_contact" value="<?php echo $teacher_contact; ?>" id="teacher_contact" class="form-control" placeholder="Enter contact number" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Birth Date</label>
                                        <div class="col-md-8">
                                            <input type="text" name="teacher_birth_date" value="<?php echo $teacher_birth_date; ?>" id="teacher_birth_date" class="form-control" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Email</label>
                                        <div class="col-md-8">
                                            <input type="text" name="teacher_email" value="<?php echo $teacher_email; ?>" id="teacher_email" class="form-control" placeholder="Enter email" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Gender</label>
                                        <div class="col-md-8">
                                            <select name="teacher_gender" id="teacher_gender" class="form-control">
                                                <option value="Male" <?php echo (($teacher_gender == 'Male')?'selected="selected"':""); ?>>Male</option>
                                                <option value="Female" <?php echo (($teacher_gender == 'Female')?'selected="selected"':""); ?> >Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Address</label>
                                        <div class="col-md-8">
                                            <textarea required="true" name="teacher_address" class="form-control" rows="2" ><?php echo $teacher_address; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <button id="save_button" type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Save</button>
                                            <a href="<?php echo site_url('admin/teachers'); ?>" class="btn btn-danger" ><i class="fa fa-reply"></i> Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/include/footer', 'refresh'); ?> 
    </div>

<?php $this->load->view('admin/include/js', 'refresh'); ?>
<script type="text/javascript">
    $('#nav_teachers').addClass('active');
    $(function() {
        
        $('#teacherForm').validate({
            rules :{
                teacher_name : {required : true},
                teacher_address : {required : true},
                teacher_contact : {required : true},
                teacher_birth_date : {required : true},
                teacher_gender : {required : true},
                teacher_email : {required : true, email : true},
            }
        });
        
        $("#teacher_birth_date").daterangepicker({        
            showDropdowns: true,
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }        
        });
    });
</script>
</body>
</html>