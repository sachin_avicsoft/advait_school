<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body class="sidebar-mini skin-red-light">
    <div class="wrapper">
        <?php $this->load->view('admin/include/header', 'refresh'); ?> 
        <?php $this->load->view('admin/include/sidebar', 'refresh'); ?> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 923px;">
          <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Dashboard
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Dashboard</li>
                </ol>
            </section>
            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
            <!-- Main content -->
            <section class="content" style="min-height: 0px;">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-user-circle"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Teachers</span>
                          <span class="info-box-number"><?php echo $teacher_count; ?></span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-id-card"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Students</span>
                          <span class="info-box-number"><?php echo $student_count; ?></span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                    </div>
        <!-- /.col -->
                    
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-th-large"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Batches</span>
                          <span class="info-box-number"><?php echo $batch_count; ?></span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                    </div>
        <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-institution"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text">Standards</span>
                          <span class="info-box-number"><?php echo $standard_count; ?></span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                      <!-- /.info-box -->
                    </div>
        <!-- /.col -->
                </div>
            </section>
            
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Advait Schools Batch Report</h3>
                            </div>
                            <div class="box-body">
                                <div>
                                    <form class="form-horizontal" >
                                        <div class="form-group">
                                            <label class="control-label">Batch Year</label>
                                            <div class="col-md-4">
                                                <select name="batch_year_id" id="batch_year_id" class="form-control" onchange="drawChart()">
                                                    <?php
                                                    if($batch_year_list){
                                                        foreach ($batch_year_list as $key => $value) {
                                                            echo '<option value="'.$value['id'].'">'.$value['batch_year'].'</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="chart_div"></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/include/footer', 'refresh'); ?> 
    </div>

<?php $this->load->view('admin/include/js', 'refresh'); ?> 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
<script type="text/javascript"> 
     
    // Load the Visualization API and the piechart package. 
    google.charts.load('current', {'packages':['corechart']}); 
       
    // Set a callback to run when the Google Visualization API is loaded. 
    google.charts.setOnLoadCallback(drawChart); 
       
    function drawChart() { 
        $('#chart_div').html('');
        var batch_year = $('#batch_year_id').val();
        var jsonData = $.ajax({ 
            url: "<?php echo site_url('admin/dashboard/getdata?batch_year='); ?>"+batch_year, 
            dataType: "json", 
            async: false 
            }).responseText; 
            
        // Create our data table out of JSON data loaded from server. 
        var data = new google.visualization.DataTable(jsonData); 
        
        var options = {
            width: 600,
            height: 400,
            vAxis:{title : 'Students'},
            hAxis:{title : 'Batches'},
            legend: { position: 'top', maxLines: 3, textStyle: {color: 'black', fontSize: 16 } },
                    isStacked: true,

                    // Displays tooltip on selection.
                    // tooltip: { trigger: 'selection' }, 
        };
        // Instantiate and draw our chart, passing in some options. 
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div')); 
        chart.draw(data, options); 
    } 
    
    
 
</script> 
</body>
</html>