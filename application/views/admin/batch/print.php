<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $setting['school_name']; ?></title> 
        <link href="<?php echo print_resource_url('/css/print.css'); ?>" rel="stylesheet">
    </head>
    <body onload="window.print();">
        <?php 
            $i;
            if ($student_list){
                for($i = 0; $i < count($student_list); $i++){
                    
                    // header
                    if($i%20 == 0){
                        ?>
                    <div class="page" style="border:solid;border-width:3px;border-color: #7D402F;margin: 15px 15px 0px 15px;">
                        <header style="margin-bottom: 0px;">
                            <img style="float: left;width: 39%;min-height: 150px;max-height: 150px" src="<?php echo base_url('uploads/'.$setting['id'].'.'.$setting['ext']); ?>" alt="School Logo" />
                            <div style="float: right;width: 60.58%;min-height: 150px;max-height: 150px">
                                <h1 style="color : #761C19;width: 100%;font-size: 50px;margin-bottom: 0px"><?php echo $setting['school_name']; ?></h1>
                                <span style="font-size: 10"><?php echo $setting['school_address']; ?><br><b>Contact : </b><?php echo $setting['school_contact']; ?></span>
                            </div>
                        </header>
                        <div id="details" class="clearfix" style="width: 100%;">
                            <div style="width: 70%; float: left;border-top: 1px solid black;margin-bottom: 10px">
                                <strong id="head" style="margin-left:25px;">Batch : </strong>
                                <?php echo $standard_name.'-'.$batch_name; ?>                  </div>
                            <div style="width: 30%; float: right;border-top: 1px solid black;margin-bottom: 10px">
                                <strong id="head">Year : </strong>
                                <?php echo $batch_year; ?>                    </div>
                            <div style="width: 70%; float: left;;margin-bottom: 10px">
                                <strong id="head" style="margin-left:25px">Teacher : </strong>
                                <?php echo $teacher_name; ?>                    </div>
                            <div style="width: 30%; float: right;;margin-bottom: 10px">
                                <strong id="head">Ph. : </strong>
                                <?php echo $teacher_contact; ?>                    </div>
                        </div>
                        <table border="1px" style="border-collapse:collapse;width: 100%;">
                            <thead style="border-bottom-color: black">
                                <tr style="background-color: #761c19;border-bottom-color: black">
                                    <th class="table" style="width: 2%;border-bottom-color: black;border-right-color: black">Sr. No.</th>
                                    <th class="table" style="width: 47%;border-bottom-color: black">Student Name</th>
                                </tr>
                            </thead>
                            <tbody style="border-bottom-color: white;"> 
                            <?php } ?>
                                <tr>
                                    <td class="table"><?php echo $i+1; ?></td>
                                    <td><?php echo $student_list[$i]['student_name']; ?></td>
                                </tr>
                            <?php 
                                        
                                if(count($student_list)-1 == $i && (($i%20 != 0) || $i==0)){
                                     $count = $i%20;
                                     $no = 19-$count;
                                     //echo $no;
                                     for($l = 0; $l < $no; $l++){
                                         echo '<tr style="height : 25px">';
                                         echo '<td class="table">'.($l+$i+2).'</td>';
                                         echo '<td class="table"></td>';
                                         echo '</tr>';
                                     } 
                                     $i= $i+$no;
                                }
                                
                                if(($i+1)%20 == 0){
                             ?> 
                            </tbody>
                        </table>
                    </div>
                <?php
                    }
                }
            }
        ?>
    </body>
</html>