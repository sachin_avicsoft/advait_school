<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body class="sidebar-mini skin-red-light">
    <div class="wrapper">
        <?php $this->load->view('admin/include/header', 'refresh'); ?> 
        <?php $this->load->view('admin/include/sidebar', 'refresh'); ?> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 923px;">
          <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Batches
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Batches</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <?php
                                        if( $id != ""){
                                            echo 'Edit Batch';
                                        } else {
                                            echo 'Add Batch';
                                        }
                                    ?>
                                </h3>
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            </div>
                            <div class="box-body">
                                <form id="batchForm" class="form form-horizontal" method="post" action="<?php echo site_url('admin/batches/save'); ?>" enctype='multipart/form-data'>
                                    
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Teacher Name</label>
                                        <div class="col-md-8">
                                            <select name="teacher_id" id="teacher_id" class="form-control">
                                                <?php
                                                if($teacher_list){
                                                    foreach ($teacher_list as $key => $value) {
                                                        if($teacher_id == $value['id']){
                                                            echo '<option selected="selected" value="'.$value['id'].'">'.$value['teacher_name'].'</option>';
                                                        } else {
                                                            echo '<option value="'.$value['id'].'">'.$value['teacher_name'].'</option>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Standard Name</label>
                                        <div class="col-md-8">
                                            <select name="standard_id" id="standard_id" class="form-control">
                                                <?php
                                                if($standard_list){
                                                    foreach ($standard_list as $key => $value) {
                                                        if($standard_id == $value['id']){
                                                            echo '<option selected="selected" value="'.$value['id'].'">'.$value['standard_name'].'</option>';
                                                        } else {
                                                            echo '<option value="'.$value['id'].'">'.$value['standard_name'].'</option>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Batch Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="batch_name" value="<?php echo $batch_name; ?>" id="batch_name" class="form-control" placeholder="Enter Batch name" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Batch Year</label>
                                        <div class="col-md-8">
                                            <select name="batch_year_id" id="batch_year_id" class="form-control">
                                                <?php
                                                if($batch_year_list){
                                                    foreach ($batch_year_list as $key => $value) {
                                                        if($batch_year_id == $value['id']){
                                                            echo '<option selected="selected" value="'.$value['id'].'">'.$value['batch_year'].'</option>';
                                                        } else {
                                                            echo '<option value="'.$value['id'].'">'.$value['batch_year'].'</option>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <button id="save_button" type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Save</button>
                                            <a href="<?php echo site_url('admin/batches'); ?>" class="btn btn-danger" ><i class="fa fa-reply"></i> Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/include/footer', 'refresh'); ?> 
    </div>

<?php $this->load->view('admin/include/js', 'refresh'); ?>
<script type="text/javascript">
    $('#nav_batches').addClass('active');
    $(function() {
        
        $('#batchForm').validate({
            rules :{
                batch_name : {required : true},
                standard_id : {required : true},
                teacher_id : {required : true},
                batch_year : {required : true},
            }
        });
    });
</script>
</body>
</html>