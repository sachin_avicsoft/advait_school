<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body class="sidebar-mini skin-red-light">
    <div class="wrapper">
        <?php $this->load->view('admin/include/header', 'refresh'); ?> 
        <?php $this->load->view('admin/include/sidebar', 'refresh'); ?> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 923px;">
          <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Batch Years
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Batch Years</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <?php
                                        if( $id != ""){
                                            echo 'Edit Batch Year';
                                        } else {
                                            echo 'Add Batch Year';
                                        }
                                    ?>
                                </h3>
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            </div>
                            <div class="box-body">
                                <form id="batchYearForm" class="form form-horizontal" method="post" action="<?php echo site_url('admin/batchyears/save'); ?>" enctype='multipart/form-data'>
                                    
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Batch Year</label>
                                        <div class="col-md-8">
                                            <input type="text" name="batch_year" value="<?php echo $batch_year; ?>" id="batch_year" class="form-control" placeholder="Enter Batch Year(17-18)" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <button id="save_button" type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Save</button>
                                            <a href="<?php echo site_url('admin/batchyears'); ?>" class="btn btn-danger" ><i class="fa fa-reply"></i> Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/include/footer', 'refresh'); ?> 
    </div>

<?php $this->load->view('admin/include/js', 'refresh'); ?>
<script type="text/javascript">
    $('#nav_batchyears').addClass('active');
    $(function() {
        
        $('#batchYearForm').validate({
            rules :{
                batch_year : {required : true},
            }
        });
    });
    
</script>
</body>
</html>