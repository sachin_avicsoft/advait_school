<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
    <style>
        table.dataTable.no-footer {
            border-bottom: 0px solid #111;
        }
    </style>
</head>
<body class="sidebar-mini skin-red-light">
    <div class="wrapper">
        <?php $this->load->view('admin/include/header', 'refresh'); ?> 
        <?php $this->load->view('admin/include/sidebar', 'refresh'); ?> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 923px;">
          <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Students
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Students</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    Student List
                                </h3>
                                <p class="clearfix">
                                    <a target="_self" class="btn btn-info pull-right" href="<?php echo site_url('admin/students/add') ?>">Add Student</a>
                                </p>
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            </div>
                            <div class="box-body">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Search By Student</label>
                                        <div class="col-md-4">
                                            <input type="text" name="search" id="inputSearch" class="form-control" />
                                        </div>
                                    </div>
                                </form>
                                <div style="width: 100%;">
                                    <div class="table-responsive">
                                        <table id="example" class="table table-striped table-hover dt-responsive display nowrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th style="width: 20px">SN</th>
                                                    <th>Student</th>
                                                    <th>Teacher</th>
                                                    <th>Standard</th>
                                                    <th>Batch</th>
                                                    <th>Contact</th>
                                                    <th style="width: 16%">Action</th>                                    
                                                </tr>
                                            </thead>
                                        </table> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/include/footer', 'refresh'); ?> 
    </div>

<?php $this->load->view('admin/include/js', 'refresh'); ?> 
<script type="text/javascript">

$(function(){
        laodStudents();
        $("#nav_students").addClass("active");
        
        var atable = $('#example').DataTable();
            $('#inputSearch').keyup( function() {
            atable.draw();
        });
    });

function laodStudents(){
    
   $('#example').DataTable( {
        "responsive":true,
        "bLengthChange": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "bFilter": true,
        "sDom":"lrtip",
        "ajax": {
            "url" :"<?php echo site_url("admin/students/list"); ?>",
            "type": "get",
            "data" : function(d){
                d.inputSearch = $('#inputSearch').val();
            },
        },        
        "columns": [
            { "data": "" },
            { "data": "student_name" },
            { "data": "teacher_name" },
            { "data": "standard_name" },
            { "data": "batch_name" },
            { "data": "student_contact" },
            { "data": "" }
        ],
        
          "columnDefs": [ 
        
        {         
            "targets": 0,
             render: function ( data, type, row, meta ) {                
                return meta.settings._iDisplayStart + meta.row + 1;
             }
         },
         
        {
            "targets": 6,
             render: function ( data, type, row, meta ) {
                 
                 var eurl ="<?php echo site_url('admin/students/edit?id=');?>" + row.id;
                 var edit = '<a target="_self" class="btn btn-xs btn-success" href="'+eurl+'" title="Edit"><li class="fa fa-edit"></li></a>';
                 var del = '<button class="btn btn-xs btn-danger"  onclick="deleteInfo('+row.id+')" title="Delete"><li class="fa fa-remove"></li></button>';
                 return edit+" "+del;
             }
        },
        
        
        
        ]
    } );
} 

function deleteInfo(id){
    BootstrapDialog.show({      
            message: 'Are you sure you want to delete this Student?',
            title : "<i class='glyphicon glyphicon-trash'></i>  Warning",
            type: BootstrapDialog.TYPE_DANGER,
            buttons: [{
                label: 'Cancel',
                action: function(dialogItself){
                    dialogItself.close();
                    dialogItself.close();
                },
                
            },{
                label: 'Delete',
                cssClass:'btn-danger',
                action: function(dialogItself){
                    res = $.ajax({type: "GET", url: "<?php echo site_url('admin/students/delete?id='); ?>"+id, async: false})    
                    $('#example').DataTable().ajax.reload();
                    dialogItself.close();
                }
            }]
        });
}            
</script>
</body>
</html>