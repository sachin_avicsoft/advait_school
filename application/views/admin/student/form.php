<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body class="sidebar-mini skin-red-light">
    <div class="wrapper">
        <?php $this->load->view('admin/include/header', 'refresh'); ?> 
        <?php $this->load->view('admin/include/sidebar', 'refresh'); ?> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 923px;">
          <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Students
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Students</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <?php
                                        if( $id != ""){
                                            echo 'Edit Student';
                                        } else {
                                            echo 'Add Student';
                                        }
                                    ?>
                                </h3>
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            </div>
                            <div class="box-body">
                                <form id="studentForm" class="form form-horizontal" method="post" action="<?php echo site_url('admin/students/save'); ?>" enctype='multipart/form-data'>
                                    
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Student Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="student_name" value="<?php echo $student_name; ?>" id="student_name" class="form-control" placeholder="Enter teacher name" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Standard Name</label>
                                        <div class="col-md-8">
                                            <select name="standard_id" id="standard_id" class="form-control" onchange="getBatchList()">
                                                <?php
                                                if($standard_list){
                                                    foreach ($standard_list as $key => $value) {
                                                        if($standard_id == $value['id']){
                                                            echo '<option selected="selected" value="'.$value['id'].'">'.$value['standard_name'].'</option>';
                                                        } else {
                                                            echo '<option value="'.$value['id'].'">'.$value['standard_name'].'</option>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Batch Name</label>
                                        <div class="col-md-8">
                                            <select name="batch_id" id="batch_id" class="form-control">
                                                <?php
                                                if($batch_list){
                                                    foreach ($batch_list as $key => $value) {
                                                        if($batch_id == $value['id']){
                                                            echo '<option selected="selected" value="'.$value['id'].'">'.$value['batch_name'].'</option>';
                                                        } else {
                                                            echo '<option value="'.$value['id'].'">'.$value['batch_name'].'</option>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Contact No</label>
                                        <div class="col-md-8">
                                            <input type="text" name="student_contact" value="<?php echo $student_contact; ?>" id="student_contact" class="form-control" placeholder="Enter contact number" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Birth Date</label>
                                        <div class="col-md-8">
                                            <input type="text" name="student_birth_date" value="<?php echo $student_birth_date; ?>" id="student_birth_date" class="form-control" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Email</label>
                                        <div class="col-md-8">
                                            <input type="text" name="student_email" value="<?php echo $student_email; ?>" id="student_email" class="form-control" placeholder="Enter email" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Gender</label>
                                        <div class="col-md-8">
                                            <select name="student_gender" id="student_gender" class="form-control">
                                                <option value="Male" <?php echo (($student_gender == 'Male')?'selected="selected"':""); ?>>Male</option>
                                                <option value="Female" <?php echo (($student_gender == 'Female')?'selected="selected"':""); ?> >Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Address</label>
                                        <div class="col-md-8">
                                            <textarea required="true" name="student_address" class="form-control" rows="2" ><?php echo $student_address; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <button id="save_button" type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Save</button>
                                            <a href="<?php echo site_url('admin/students'); ?>" class="btn btn-danger" ><i class="fa fa-reply"></i> Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/include/footer', 'refresh'); ?> 
    </div>

<?php $this->load->view('admin/include/js', 'refresh'); ?>
<script type="text/javascript">
    $('#nav_students').addClass('active');
    $(function() {
        
        $('#studentForm').validate({
            rules :{
                student_name : {required : true},
                student_address : {required : true},
                student_contact : {required : true},
                student_birth_date : {required : true},
                student_gender : {required : true},
                batch_id : {required : true},
                student_email : { email : true },
            }
        });
        
        $("#student_birth_date").daterangepicker({        
            showDropdowns: true,
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }        
        });
    });
    
    function getBatchList(){
        var standard_id = $('#standard_id').val();
        $.ajax({
            type : 'GET',
            dataType : 'json',
            url : '<?php echo site_url("admin/batches/getbatches?standard_id=");?>'+standard_id,
            error : function(){
                console.log('error getting list');
            },
            success : function (res){
                var batchList = res;
                $('#batch_id').find('option').remove();
                for(var i = 0; i < batchList.length; i++){
                    $('#batch_id').append('<option value="'+batchList[i].id+'">'+batchList[i].batch_name+'</option>');
                }
            }
        });
    }
</script>
</body>
</html>