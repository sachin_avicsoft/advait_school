<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('admin/include/css', 'refresh'); ?>    
</head>
<body class="sidebar-mini skin-red-light">
    <div class="wrapper">
        <?php $this->load->view('admin/include/header', 'refresh'); ?> 
        <?php $this->load->view('admin/include/sidebar', 'refresh'); ?> 

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 923px;">
          <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    School Info
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">School Info</li>
                </ol>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <?php
                                        if( $id != ""){
                                            echo 'School Info';
                                        } else {
                                            echo 'School Info';
                                        }
                                    ?>
                                </h3>
                                <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            </div>
                            <div class="box-body">
                                <form id="schoolForm" class="form form-horizontal" method="post" action="<?php echo site_url('admin/settings/save'); ?>" enctype='multipart/form-data'>
                                    
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" class="form-control"/>
                                    
                                    <div class="form-group">
                                        <label class="control-label col-md-2">School Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="school_name" value="<?php echo $school_name; ?>" id="school_name" class="form-control" placeholder="Enter School Name" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">School Contact No</label>
                                        <div class="col-md-8">
                                            <input type="text" name="school_contact" value="<?php echo $school_contact; ?>" id="school_contact" class="form-control" placeholder="Enter School contact number" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">School Address</label>
                                        <div class="col-md-8">
                                            <textarea required="true" name="school_address" class="form-control" rows="2" ><?php echo $school_address; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Upload Logo</label>
                                        <div class="col-md-8">
                                            <input id="file" name="userfile[]" size="20" type="file" onchange="return ValidateFileUpload()" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-offset-2">
                                            <div style="height: 120px; width: 120px;margin-left:10px">
                                                <?php if(isset($imageFile) && $imageFile && count($imageFile) > 0) { ?>
                                                    <img class="" src="<?php echo base_url('uploads/'.$imageFile['id'].'.'.$imageFile['ext']);?>" id="blah" style="width : 100px; height: 100px; " />
                                                <?php } else { ?>
                                                    <img class="" src="<?php echo base_url('images/not-available.jpg');?>" id="blah" style="width : 100px; height: 100px; margin:-10px" />
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-10">
                                            <button id="save_button" type="submit" class="btn btn-success" ><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('admin/include/footer', 'refresh'); ?> 
    </div>

<?php $this->load->view('admin/include/js', 'refresh'); ?>
<script type="text/javascript">
    $('#nav_settings').addClass('active');
    $(function() {
        
        $('#schoolForm').validate({
            rules :{
                school_name : {required : true},
                school_address : {required : true},
                school_contact : {required : true},
            }
        });
    });
    
    //Upload functions
    function ValidateFileUpload() {
        var fuData = document.getElementById('file');
        var FileUploadPath = fuData.value;

    //To check if user upload any file
        if (FileUploadPath == '') {
            alert("Please upload an image");
        } else {
            var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

    //The file uploaded is an image
        if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {

    // To Display
                if (fuData.files && fuData.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                }

            } 

    //The file upload is NOT an image
        else {
                alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");

            }
        }
    }
</script>
</body>
</html>