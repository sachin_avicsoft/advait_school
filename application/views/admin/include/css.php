<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>Advait School Management</title>

<link type="text/css" rel="stylesheet" href="<?php print_resource_url('css/font-awesome.min.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('css/selectize.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('admin/bootstrap/css/bootstrap.min.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('admin/dist/css/AdminLTE.min.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('admin/dist/css/skins/skin-red-light.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('admin/dist/css/skins/skin-red.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('admin/plugins/jQueryUI/jquery-ui.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('admin/plugins/datatables/dataTables.jqueryui.min.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php print_resource_url('admin/plugins/datatables/jquery.dataTables.min.css'); ?>">