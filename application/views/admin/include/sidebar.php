
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height: auto;">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                
                <li id="nav_teachers">
                    <a href="<?php echo site_url("admin/teachers"); ?>">
                        <i class="fa fa-user-circle"></i> 
                        <span>Teachers</span>
                    </a>
                </li>
                <li id="nav_students">
                    <a href="<?php echo site_url("admin/students"); ?>">
                        <i class="fa fa-id-card"></i> 
                        <span>Students</span>
                    </a>
                </li>
                <li id="nav_standards">
                    <a href="<?php echo site_url("admin/standards"); ?>">
                        <i class="fa fa-institution"></i> 
                        <span>Standards</span>
                    </a>
                </li>
                <li id="nav_batches">
                    <a href="<?php echo site_url("admin/batches"); ?>">
                        <i class="fa fa-th-large"></i> 
                        <span>Batches</span>
                    </a>
                </li>
                <li id="nav_batchyears">
                    <a href="<?php echo site_url("admin/batchyears"); ?>">
                        <i class="fa fa-clock-o"></i> 
                        <span>Batch Years</span>
                    </a>
                </li>
                <li id="nav_batches">
                    <a href="<?php echo site_url("admin/settings"); ?>">
                        <i class="fa fa-cog"></i> 
                        <span>School Setting</span>
                    </a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>    

