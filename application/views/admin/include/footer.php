<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.7
    </div>
    <strong>Copyright © 2017 <a href="http://avicsoft.com">AvicSoft</a>.</strong> All rights reserved.
</footer>