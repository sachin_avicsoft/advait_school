<!DOCTYPE HTML>
<html>
<head>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <?php $this->load->view('include/css.php');?>
</head>
<body>
	<!--start-home-->
	<!----- start-header---->
    <div id="home" class="header two">
        <div class="top-header two">	
            <div class="container">
                <div class="logo">
                    <a href="<?php echo site_url(); ?>"><h1>Advait<span>School</span></h1></a>
                </div>
                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul class="cl-effect-16">
                        <li><a href="<?php echo site_url('/'); ?>" data-hover="Home">Home</a></li>
                        <li><a href="<?php echo site_url('about'); ?>" data-hover="About">About</a></li>
                        <li><a href="<?php echo site_url('teachers'); ?>" data-hover="Teachers">Teachers</a></li>
                        <li><a class="active" href="<?php echo site_url('gallery'); ?>" data-hover="Gallery">Gallery</a></li>
                        <li><a href="<?php echo site_url('contact'); ?>" data-hover="Contact">Contact</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
		<!----- //End-slider---->
<!--gallery-->
    <div class="gallery services">
        <div class="container">
            <div class="gallery-head">
                <h3>Gallery</h3>
                <p>Our kids Gallery</p>
            </div>
            <div class="portfolio-bottom">
                <div class="gallery-one two">
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/g1.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/g1.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/n1.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/n1.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/g2.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/g2.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/g3.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/g3.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/n2.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/n2.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/g4.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/g4.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/g2.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/g2.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/g5.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/g5.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                    <div class="col-md-4 gallery-left two">
                        <a href="<?php echo print_resource_url('web/images/n3.jpg'); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="<?php echo print_resource_url('web/images/n3.jpg'); ?>" alt="" class="img-responsive zoom-img"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--/mid-bg-->
    <div class="mid-bg">
        <div class="container">
            <div class="mid-section">
                <h3>First Day at School!</h3>
                <h4>ARE YOU READY ?</h4>
                <p>Welcome to school days.</p>
            </div>
        </div>
    </div>
    <?php $this->load->view('include/footer.php');?>
    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <?php $this->load->view('include/js.php');?>
<script type="text/javascript">
    $("span.menu").click(function(){
        $(".top-menu ul").slideToggle("slow" , function(){
        });
    });
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
    
    jQuery(function($) {
        $(".swipebox").swipebox();
    });
    
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
    
    $(document).ready(function() {
        /*
        var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
        };
        */

        //$().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
</body>
</html>