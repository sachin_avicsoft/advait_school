<!DOCTYPE HTML>
<html>
<head>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <?php $this->load->view('include/css.php');?>
</head>
<body>
	<!--start-home-->
	<!----- start-header---->
    <div id="home" class="header two">
        <div class="top-header two">	
            <div class="container">
                <div class="logo">
                    <a href="<?php echo site_url(); ?>"><h1>Advait<span>School</span></h1></a>
                </div>
                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul class="cl-effect-16">
                        <li><a href="<?php echo site_url('/'); ?>" data-hover="Home">Home</a></li>
                        <li><a href="<?php echo site_url('about'); ?>" data-hover="About">About</a></li>
                        <li><a href="<?php echo site_url('teachers'); ?>" data-hover="Teachers">Teachers</a></li>
                        <li><a href="<?php echo site_url('gallery'); ?>" data-hover="Gallery">Gallery</a></li>
                        <li><a class="active" href="<?php echo site_url('contact'); ?>" data-hover="Contact">Contact</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!----- //End-slider---->
    <!--/contact-->
    <div class="contact">
        <div class="container">
            <div class="contact-head">
                <h3>Contact Us</h3>
                <p>Keep on tuch with me</p>
            </div>
            <div class="contact-top">
                <div class="col-md-4 contact-text">
                    <div class="cont-grid">
                        <div class="con-icon">
                            <i class="loca"> </i>
                        </div>
                        <div class="con-text">
                            <h4>MAIN SCHOOL</h4>
                            <p>Rohakal Rd,Pune Nashik Highway, </p>
                            <p>Opp. Green Estate,Chakan,</p>
                            <p>Tal.Khed, Dist.Pune-410501</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="cont-grid">
                            <div class="con-icon">
                                    <i class="net"> </i>
                            </div>
                            <div class="con-text">
                                    <h4>School Contact</h4>
                                    <p>+91 9822573347</p>
                            </div>
                            <div class="clearfix"> </div>
                    </div>
                    <div class="cont-grid">
                            <div class="con-icon">
                                    <i class="mail"> </i>
                            </div>
                            <div class="con-text">
                                    <h4>M@IL US</h4>
<!--                                    <p>advaitschool9444@gmail.com</p>-->
                                    <p>contactus@advaitschoolchakan.in</p>
                            </div>
                            <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-8 contact-form">
                    <form method="post" action="<?php echo site_url('contact/send');?>">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="text" class="textbox" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                        <input type="text" class="textbox" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
                        <textarea value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                        <input type="submit" value="Send Now">
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
	</div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJZUA0_xjKwjsRcJmic16AzeM&key=AIzaSyArZyx5-uMbRvGfjown969NOWMP4R0XfOc"> </iframe>
        </div>
    </div>
    <?php $this->load->view('include/footer.php');?>
    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <?php $this->load->view('include/js.php');?>
<script type="text/javascript">
    $("span.menu").click(function(){
        $(".top-menu ul").slideToggle("slow" , function(){
        });
    });
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
    
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
    
    $(document).ready(function() {
        /*
        var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
        };
        */

        //$().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
</body>
</html>