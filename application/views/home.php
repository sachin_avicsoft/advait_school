<!DOCTYPE HTML>
<html>
<head>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <?php $this->load->view('include/css.php');?>
</head>
<body>
    <!----- start-header---->
    <div id="home" class="header">
        <div class="top-header">
            <div class="container">
                <div class="logo">
                    <a href="<?php echo site_url(); ?>"><h1>Advait<span>School</span></h1></a>
                </div>
                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul class="cl-effect-16">
                        <li><a class="active" href="<?php echo site_url('/'); ?>" data-hover="Home">Home</a></li>
                        <li><a  href="<?php echo site_url('about'); ?>" data-hover="About">About</a></li>
                        <li><a href="<?php echo site_url('teachers'); ?>" data-hover="Teachers">Teachers</a></li>
                        <li><a href="<?php echo site_url('gallery'); ?>" data-hover="Gallery">Gallery</a></li>
                        <li><a href="<?php echo site_url('contact'); ?>" data-hover="Contact">Contact</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
<!--- banner Slider starts Here --->
            <!----//End-slider-script---->
            <!-- Slideshow 4 -->
        <div  id="top" class="callbacks_container">
            <ul class="rslides" id="slider4">
                <li>
                    <div class="slider-top">
                        <h2>Knowledge is Power</h2>
                        <p>Knowledge is power. You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it  You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it..</p>
                        <h6>Welcoming for every Child</h6>
                    </div>
                </li>
                <li>
                    <div class="slider-top">
                        <h2>Perfect Education</h2>
                        <p>Knowledge is power. You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it  You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it..</p>
                        <h6>Welcoming for every Child</h6>
                    </div>
                </li>
                <li>
                    <div class="slider-top">
                        <h2>School Education</h2>
                        <p>Knowledge is power. You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it  You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it..</p>
                        <h6>Welcoming for every Child</h6>
                    </div>
                </li>
                <li>
                    <div class="slider-top">
                        <h2>Perfect Education</h2>
                        <p>Knowledge is power. You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it  You can't begin a career, for that matter even a relationship, unless you know everything there is to know about it..</p>
                        <h6>Welcoming for every Child</h6>
                   </div>
                </li>
            </ul>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!----- //End-slider---->
    <!----start-slide-bottom--->
    <div class="slide-bottom">
        <div class="slide-bottom-grids">
            <div class="container">
                <div class="col-md-6 slide-bottom-grid">
                    <h3>Welcome!</h3>
                    <p>Top schools in Pune have always attracted students from all over the country and abroad, and in such an environment The Advait School is a preferred choice of parents seeking admission to good schools in Pune.</p>
                </div>
                <div class="col-md-6 slide-bottom-grid">
                    <h3>Our Mission</h3>
                    <p>The school mission is to encourage the natural curiosity of young minds and inculcate a lifelong habit of   “continuous learning” in every child. Learning, of course, is not to be restricted to the classrooms, but continues outside: on the play field, in gardens and just about everywhere.</p>
                </div>
                <div class="clearfix"></div>
            </div>
         </div>
    </div>
    <!--services-->
    <div class="service-section">
        <div class="col-md-7 service-section-grids">
            <div class="container">
                <div class="serve-head">
                    <h3>Our Activities</h3>
                    <h6>Our Bestservices for your Kids</h6>
                </div>
            </div>
            <div class="service-grid">
                <div class="service-section-grid">
                    <div class="icon">
                        <i class="book"> </i>
                    </div>
                    <div class="icon-text">
                        <h4>BOOKS STATIONARY</h4>
                        <p>
Library

Books are our friend and philosopher for life. The more you read, the more things you will know. The more you learn the more places you will go. To inculcate the habit of reading at Advait School, we have a library period right from Kindergarten onwards.</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="service-section-grid">
                    <div class="icon">
                        <i class="book"> </i>
                    </div>
                    <div class="icon-text">
                        <h4>DANCE AND MUSIC ROOMS</h4>
                        <p>Dance rooms for Indian Classical Dances (Kathak and Bharat Natyam) to focus on rhythm and movement.Music rooms wherein children learn vocal music and learn to play musical instruments like Tabla and Keyboard.</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="service-section-grid">
                    <div class="icon">
                        <i class="book"> </i>
                    </div>
                    <div class="icon-text">
                        <h4>YOGA AND ART ROOMS</h4>
                        <p>Yoga room covered with soft mats for conducting yoga classes for flexibility and concentration.Art rooms are designed so welled that childeren can easily express their ideas.</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="col-md-5 service-text">
            <p></p>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!--/services-->
<!--    <div class="news-section">
        <div class="container">
            <div class="news-head">
                <h3>Current News</h3>
                <p>Follow our Most Important News</p>
            </div>
            <div class="news">
                <div class="col-md-4 test-right01 test1">
                    <img src="<?php echo print_resource_url('web/images/n1.jpg'); ?>" class="img-responsive" alt="" />
                    <div class="textbox textbox1">
                        <h4 class="col-md-4 date">14<br> <span>Jun</span><br><lable>0 <img src="<?php echo print_resource_url('web/images/comment.png'); ?>" class="img-responsive" alt="" /></lable></h4>
                        <p class="col-md-8 news">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit...</p>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-4 test-right01 test1">
                    <img src="<?php echo print_resource_url('web/images/n2.jpg'); ?>" class="img-responsive" alt="" />
                    <div class="textbox textbox1">
                        <h4 class="col-md-4 date">14<br> <span>Jun</span><br><lable>0 <img src="<?php echo print_resource_url('web/images/comment.png'); ?>" class="img-responsive" alt="" /></lable></h4>
                        <p class="col-md-8 news">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit...</p>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-4 test-right01 test1">
                    <img src="<?php echo print_resource_url('web/images/n3.jpg'); ?>" class="img-responsive" alt="" />
                    <div class="textbox textbox1">
                        <h4 class="col-md-4 date">14<br> <span>Jun</span><br><lable>0 <img src="<?php echo print_resource_url('web/images/comment.png'); ?>" class="img-responsive" alt="" /></lable></h4>
                        <p class="col-md-8 news">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed blandit...</p>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>-->
    <div class="culture-section">
        <div class="container">
            <div class="culture-head">
                <h3>Our Events</h3>
                <p>Don'T Miss our Events</p>
            </div>
            <div class="culture">
                <div class="col-md-6 culture-grids">
                    <a href="<?php echo site_url('single'); ?>"> <img src="<?php echo print_resource_url('web/images/event1.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="<?php echo site_url('single'); ?>"><h5>PARTY SESSION</h5></a>
                    <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>-->
                </div>
                <div class="col-md-6 culture-grids">
                    <a href="<?php echo site_url('single'); ?>"> <img src="<?php echo print_resource_url('web/images/event2.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="<?php echo site_url('single'); ?>"><h5>CLASS SESSION</h5></a>
                    <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>-->
                </div>
                <div class="col-md-6 culture-grids">
                    <a href="<?php echo site_url('single'); ?>"> <img src="<?php echo print_resource_url('web/images/event3.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="<?php echo site_url('single'); ?>"><h5>ART SESSION</h5></a>
                    <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>-->
                </div>
                <div class="col-md-6 culture-grids">
                    <a href="<?php echo site_url('single'); ?>"> <img src="<?php echo print_resource_url('web/images/event4.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="e_date">
                        <h4>15<br> <span>July</span></h4>
                    </div>
                    <a href="<?php echo site_url('single'); ?>"><h5>ACTIVITY SESSION</h5></a>
                    <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>-->
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="mid-bg">
        <div class="container">
            <div class="mid-section">
                <h3>First Day at School!</h3>
                <h4>ARE YOU READY ?</h4>
                <p>Welcome to school days.</p>
            </div>
        </div>
    </div>
    <?php $this->load->view('include/footer.php');?>
    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <?php $this->load->view('include/js.php');?>
<script type="text/javascript">
    $("span.menu").click(function(){
        $(".top-menu ul").slideToggle("slow" , function(){
        });
    });
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
    
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
    
    $(document).ready(function() {
        /*
        var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
        };
        */

        //$().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
</body>
</html>