<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php $this->load->view('include/css', 'refresh'); ?>  
</head>
<body>
    <div class="wrapper backimage" style="min-height: 100%;">    
        <div class="container ">
            <div class="clearfix">
                
                <div class="row">
                    
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        
                        <div class="login-box-body whiteback background-grey">
                            
                            
                            <?php if($this->session->flashdata('msg')) { echo $this->session->flashdata('msg'); } ?>
                            
                            <div id="login" >
                                <div class="panel panel-default">
                                    <div class="panel-heading login-heading" style="background-color: #dd4b39">
                                        <span class="login-box-msg">Login</span> 
<!--                                        <span><a onclick="showregister()" style="color : #FFFFFF" class="btn btn-link" target="_self">Register</a></span>-->
                                    </div>
                                    <div class="panel-body">
                                        <form id="loginForm" class="form" method="post" accept-charset="utf-8" action="<?php echo site_url("login") ?>">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <input type="hidden" value="login" name="action" />

                                            <div class="form-group">
                                                <label class="control-label">User Name:</label>
                                                <div class="">
                                                    <input type="text" name="username" class="form-control" placeholder="Enter User Name" />
                                                    <span class="error"><?php echo form_error('username'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Password:</label>
                                                <div class="">
                                                    <input type="password" name="password" class="form-control" placeholder="Enter Password" />
                                                    <span class="error"><?php echo form_error('password'); ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">            
                                                <div class="">                                                                                                    
                                                    <button class="btn" style="background-color: #dd4b39 ; color: #FFFFFF" type="submit">Sign In</button>
                                                </div>                                               
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    
                    <div class="col-md-4"></div>                   
                </div>   
            </div>
            
        </div>
    </div>  

<?php  $this->load->view('include/js', 'refresh'); ?>

<script type="text/javascript">
$('#nav-login').addClass('active');


$(function(){
    $("#loginForm").validate({
        rules : {
            user_name : {required :true},
            password : {required :true},
        }
    });
    
});

</script>
</body>
</html>
