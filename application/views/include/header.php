<header id="home">
    <section class="top-nav hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="top-left">
                        +91 9028455883 / +91 9028332951
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="top-right">
                        <p>Location:<span>Baner, Pune</span></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="main-nav">
        <nav class="navbar">
            <div class="container">
                
                <div class="navbar-header">
                    
                    <a title="Avicsoft It Solutions" href="<?php echo site_url("/"); ?>" class="navbar-brand">                        
                        <img src="<?php echo print_resource_url("/images/logo_1.png"); ?>">
                    </a>
                    
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ftheme">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                    
                    </button>
                    
                </div>

                <div class="navbar-collapse collapse" id="ftheme">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo site_url("/#home"); ?>">home</a></li>
                        <li><a href="<?php echo site_url("/#about"); ?>">about</a></li>
                        <li><a href="<?php echo site_url("/#service"); ?>">services</a></li>
                        <li><a href="<?php echo site_url("/#portfolio"); ?>">portfolio</a></li>
                        <li><a href="<?php echo site_url("/#contact"); ?>">contact</a></li>
                    </ul>
                </div>
                
            </div>
        </nav>
    </div>

</header>