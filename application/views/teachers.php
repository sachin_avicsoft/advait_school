<!DOCTYPE HTML>
<html>
<head>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <?php $this->load->view('include/css.php');?>
    <style>
            .flist li:before { content:"\2714\0020"; }
    </style>
</head>
<body>
	<!--start-home-->
	<!----- start-header---->
    <div id="home" class="header two">
        <div class="top-header two">	
            <div class="container">
                <div class="logo">
                    <a href="<?php echo site_url(); ?>"><h1>Advait<span>School</span></h1></a>
                </div>
                <div class="top-menu">
                    <span class="menu"> </span>
                    <ul class="cl-effect-16">
                        <li><a href="<?php echo site_url('/'); ?>" data-hover="Home">Home</a></li>
                        <li><a href="<?php echo site_url('about'); ?>" data-hover="About">About</a></li>
                        <li><a class="active" href="<?php echo site_url('teachers'); ?>" data-hover="Teachers">Teachers</a></li>
                        <li><a href="<?php echo site_url('gallery'); ?>" data-hover="Gallery">Gallery</a></li>
                        <li><a href="<?php echo site_url('contact'); ?>" data-hover="Contact">Contact</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
		<!----- //End-slider---->
<!---start-history--->
    <div class="history">
        <div class="container">
            <div class="col-md-6">
            <div class="history-head ">
                <h3>Our Features</h3>
                <p>A Few words about us</p>
             </div>
            <div class="">
                <ul class="fa-ul flist" style="list-style-type : none">
                    <li >Well Qualified and experienced Staff </li>
                    <li class="fa fa-check">Playway method in teaching for playgroup,Jr.KG and Sr.KG.</li>
                    <li class="fa fa-check">A specious well planned and professionally Managed Garden.</li>
                    <li class="fa fa-check">Special Extra curricular activities i.e. Karate, Singing, Dance, Drama, Drawing, Carrom, Chess etc. </li>
                    <li class="fa fa-check">Focus on all round development of the student. </li>
                    <li class="fa fa-check">Special School Bus/Van Facility</li>
                    <li class="fa fa-check">Safe and Scientific Physical Education.</li>
                    <li class="fa fa-check">Hindi and Marathi Language is also teached.Playgroup and sanskrit also teached in school.</li>
                    <li class="fa fa-check">Sick by along with doctors check.</li>
                    <li class="fa fa-check">Visits and Excursions to various Places.</li>
                    <li class="fa fa-check">Annual Gathering, Sports Day, Other Celebrations</li>
                    <li class="fa fa-check">Annual Picnic, Parents Meeting in Every Three Months</li>
                    <li class="fa fa-check">Clean and green environment with playground.</li>
                    <li class="fa fa-check">Yoga, Meditation and self discipline development sessions.</li>
                </ul>
            </div>
            </div>
            <div class="col-md-6">
            <div class="history-head">
                <h3>OUR CLASSES</h3>
                <p>A Few things about classes</p>
             </div>
            <div class="col-md-6 ">
                <ul class="flist" style="list-style-type : none">
                    <li>Play Group</li>
                    <li>Jr. KG</li>
                    <li>Sr. KG</li>
                    <li>Std. 1</li>
                    <li>Std. 2</li>
                    <li>Std. 3</li>
                    <li>Std. 4</li>
                </ul>
            </div>
            </div>
            <div class="history-faq-grids">
<!--                <div class="col-md-6 history-faq-grid">
                    <h4>Our Features</h4>
                    
                    <div class="sap_tabs">	
                        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                            <ul class="resp-tabs-list">
                                <li class="resp-tab-item grid1" aria-controls="tab_item-0" role="tab"><span>1</span></li>
                                <li class="resp-tab-item grid2" aria-controls="tab_item-1" role="tab"><span>2</span></li>
                                <li class="resp-tab-item grid3" aria-controls="tab_item-2" role="tab"><span>3</span></li>
                                <li class="resp-tab-item grid4" aria-controls="tab_item-3" role="tab"><span>4</span></li>
                                <li class="resp-tab-item grid5" aria-controls="tab_item-4" role="tab"><span>5</span></li>
                                <div class="clear"></div>
                            </ul>				  	 
                            <div class="resp-tabs-container">
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                                    <div class="facts">
                                        <ul class="tab_list">
                                            <li>Well Qualified and experienced Staff </li>
                                            <li>Playway method in teaching for playgroup,Jr.KG and Sr.KG.</li>
                                            <li>A specious well planned and professionally Managed Garden.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
                                    <div class="facts">
                                        <ul class="tab_list">
                                            <li>Special Extra curricular activities i.e. Karate, Singing, Dance, Drama, Drawing, Carrom, Chess etc. </li>
                                            <li>Focus on all round development of the student. </li>
                                            <li>Special School Bus/Van Facility</li>
                                            <li>Safe and Scientific Physical Education.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
                                    <div class="facts">
                                        <ul class="tab_list">
                                            <li>Hindi and Marathi Language is also teached.Playgroup and sanskrit also teached in school.</li>
                                            <li>Sick by along with doctors check.</li>
                                            <li>Visits and Excursions to various Places.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-3">
                                    <div class="facts">
                                        <ul class="tab_list">
                                            <li>Teaching as per SSC and CBSC Curriculum.<br/>Well equipped Library and Toy room.</li>
                                            <li>Proeity for indoor as well outdoor games.</li>
                                            <li>Seminar and guest lectures for personality development.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
                                    <div class="facts">
                                        <ul class="tab_list">
                                            <li>Annual Gathering, Sports Day, Other Celebrations<br/>Annual Picnic, Parents Meeting in Every Three Months</li>
                                            <li>Clean and green environment with playground.</li>
                                            <li>Yoga, Meditation and self discipline development sessions.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
<!--                <div class="col-md-6 history-text-grid">
                    <h4>OUR CLASSES</h4>
                    <div class="achiv">
                        <lable class="one">1.</lable>
                        <p class="para">Play Group</p>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="achiv">
                        <lable class="one">2.</lable>
                        <p class="para">Jr. KG</p>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="achiv">
                        <lable class="one">3.</lable>
                        <p class="para">Sr. KG</p>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="achiv">
                        <lable class="one">4.</lable>
                        <p class="para">Std. 1 to Std. 4</p>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>-->
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

 
<!-- //activities -->
<!--    <div class="member-section">
        <div class="container">
            <div class="member-head">
                <h3>Our Teachers</h3>
                <p>2017 Board Members</p>
            </div>
            <div class="members">
               <div class="col-md-4 member-grids">
                   <a href="#"> <img src="<?php echo print_resource_url('web/images/m2.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <h5>President</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="col-md-4 member-grids">
                    <a href="#"> <img src="<?php echo print_resource_url('web/images/m3.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <h5>Vice President</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="col-md-4 member-grids">
                    <a href="#"> <img src="<?php echo print_resource_url('web/images/m1.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <h5>Chief of Staff</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>-->
<!--/mid-bg-->
    <div class="mid-bg">
        <div class="container">
            <div class="mid-section">
                <h3>First Day at School!</h3>
                <h4>ARE YOU READY ?</h4>
                <p>Welcome to school days.</p>
            </div>
        </div>
    </div>
    <?php $this->load->view('include/footer.php');?>
    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <?php $this->load->view('include/js.php');?>
<script type="text/javascript">
    $("span.menu").click(function(){
        $(".top-menu ul").slideToggle("slow" , function(){
        });
    });
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
    
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true   // 100% fit in a container
        });
    });
    
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
    
    $(document).ready(function() {
        /*
        var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
        };
        */

        //$().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
</body>
</html>