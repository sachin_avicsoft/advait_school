<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @property User $User
 * @property Student $Student
 * @property Batch $Batch
 * @property Standard $Standard
 * @property Teacher $Teacher
 * @property Setting $Setting
 * @property BatchYear $BatchYear
 */

class MY_Controller extends CI_Controller {

    public function __construct() {       
        parent::__construct();     
    }
    
    var $SESSION_UESR_ID;
    
    public function checkLogin(){        
        if(!$this->session->userdata('logged_in')){              
            redirect('login');
        } else {
            $userdata = $this->session->userdata('logged_in'); 
            $this->SESSION_UESR_ID = $userdata['user_id']; 
        }
    }
    
    public function getSessionUserId(){
        return $this->SESSION_UESR_ID;
    }  
}
?>