<?php

class Setting extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function add(){
        $data = array(
            'school_name' => $this->input->post('school_name'),
            'school_address' => $this->input->post('school_address'),
            'school_contact' => $this->input->post('school_contact'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('settings',$data);
        return $this->db->insert_id();
    }
    
    public function update(){
        $data = array(
            'school_name' => $this->input->post('school_name'),
            'school_address' => $this->input->post('school_address'),
            'school_contact' => $this->input->post('school_contact'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('settings',$data);
    }
    
    public function get_data(){
        $this->db->select()->from('settings');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    
    public function insert_file($filename, $type, $ext, $id) {
        $data = array(
            'fileName' => $filename,
            'contentType' => $type,
            'ext' => $ext,
        );
        $this->db->where('id',$id);
        $this->db->update('settings',$data);
    }
    
    public function updateLocation($id, $location) {
        $data = array('location' => $location,);
        $this->db->where('id', $id);
        $this->db->update('settings', $data);
    }
    
}
