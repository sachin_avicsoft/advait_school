<?php

class Standard extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function add(){
        $data = array(
            'standard_name' => $this->input->post('standard_name'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('standards',$data);
        return $this->db->insert_id();
    }
    
    public function update(){
        $data = array(
            'standard_name' => $this->input->post('standard_name'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('standards',$data);
    }
    
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('standards',$data);
    }

    public function get($id) {
        $this->db->select()->from('standards');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    
    public function get_standard_list(){
        $this->db->select()->from('standards');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function standardList($limit, $start){
        
        $data = array();        
        $this->db->limit($limit, $start);
        $this->db->select('standards.*')->from('standards');   
        $this->db->where('active', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result_array();
        } else {
            $data['data'] = array();
        }
        
        $this->db->select('count(*) as ncount');
        $this->db->from('standards');   
        $this->db->where('active', 1);
        $query = $this->db->get();
        $count = 0;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $count = $row['ncount'];
        }
        $data['recordsFiltered']= $count;
        $data['recordsTotal'] = $count;        
        return $data;
    }
}
