<?php

class BatchYear extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function add(){
        $data = array(
            'batch_year' => $this->input->post('batch_year'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('batch_years',$data);
        return $this->db->insert_id();
    }
    
    public function update(){
        $data = array(
            'batch_year' => $this->input->post('batch_year'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('batch_years',$data);
    }
    
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('batch_years',$data);
    }

    public function get($id) {
        $this->db->select()->from('batch_years');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    
    public function get_batch_year_list(){
        $this->db->select()->from('batch_years');
        $this->db->where('active', 1);
        $this->db->order_by('batch_year','desc');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
public function batchYearList($limit, $start){
        
        $data = array();        
        $this->db->limit($limit, $start);
        $this->db->select('batch_years.*')->from('batch_years');  
        $this->db->where('batch_years.active', 1);
        $this->db->order_by('batch_year','desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result_array();
        } else {
            $data['data'] = array();
        }
        
        $this->db->select('count(*) as ncount');
        $this->db->from('batch_years'); 
        $this->db->where('batch_years.active', 1);
        $this->db->order_by('batch_year','desc');
        $query = $this->db->get();
        $count = 0;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $count = $row['ncount'];
        }
        $data['recordsFiltered']= $count;
        $data['recordsTotal'] = $count;        
        return $data;
    }
}
