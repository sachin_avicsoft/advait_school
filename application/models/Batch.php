<?php

class Batch extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function add(){
        $data = array(
            'standard_id' => $this->input->post('standard_id'),
            'teacher_id' => $this->input->post('teacher_id'),
            'batch_year_id' => $this->input->post('batch_year_id'),
            'batch_name' => $this->input->post('batch_name'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('batches',$data);
        return $this->db->insert_id();
    }
    
    public function update(){
        $data = array(
            'standard_id' => $this->input->post('standard_id'),
            'teacher_id' => $this->input->post('teacher_id'),
            'batch_year_id' => $this->input->post('batch_year_id'),
            'batch_name' => $this->input->post('batch_name'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('batches',$data);
    }
    
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('batches',$data);
    }

    public function get($id) {
        $this->db->select()->from('batches');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    
    public function get_batch_list(){
        $this->db->select()->from('batches');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function get_all_batch_student_report_by_batch_year($batch_year_id){
        $this->db->select('batches.id,batches.batch_name,standards.standard_name')->from('batches');
        $this->db->join('standards','batches.standard_id = standards.id','left outer');
        $this->db->where('batches.active', 1);
        $this->db->where('batches.batch_year_id', $batch_year_id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function get_batch_list_by_standard_id($standard_id){
        $this->db->select()->from('batches');
        $this->db->where('active', 1);
        $this->db->where('standard_id', $standard_id);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function batchList($limit, $start){
        
        $data = array();        
        $this->db->limit($limit, $start);
        $this->db->select('batches.*,standards.standard_name,teachers.teacher_name,batch_years.batch_year')->from('batches');   
        $this->db->join('standards','batches.standard_id = standards.id');
        $this->db->join('batch_years','batches.batch_year_id = batch_years.id');
        $this->db->join('teachers','batches.teacher_id = teachers.id');
        $this->db->where('batches.active', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result_array();
        } else {
            $data['data'] = array();
        }
        
        $this->db->select('count(*) as ncount');
        $this->db->from('batches'); 
        $this->db->join('standards','batches.standard_id = standards.id');
        $this->db->join('batch_years','batches.batch_year_id = batch_years.id');
        $this->db->join('teachers','batches.teacher_id = teachers.id');
        $this->db->where('batches.active', 1);
        $query = $this->db->get();
        $count = 0;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $count = $row['ncount'];
        }
        $data['recordsFiltered']= $count;
        $data['recordsTotal'] = $count;        
        return $data;
    }
}
