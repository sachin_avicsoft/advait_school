<?php

class Teacher extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function add(){
        $data = array(
            'teacher_name' => $this->input->post('teacher_name'),
            'teacher_address' => $this->input->post('teacher_address'),
            'teacher_contact' => $this->input->post('teacher_contact'),
            'teacher_gender' => $this->input->post('teacher_gender'),
            'teacher_birth_date' => $this->input->post('teacher_birth_date'),
            'teacher_email' => $this->input->post('teacher_email'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('teachers',$data);
        return $this->db->insert_id();
    }
    
    public function update(){
        $data = array(
            'teacher_name' => $this->input->post('teacher_name'),
            'teacher_address' => $this->input->post('teacher_address'),
            'teacher_contact' => $this->input->post('teacher_contact'),
            'teacher_gender' => $this->input->post('teacher_gender'),
            'teacher_birth_date' => $this->input->post('teacher_birth_date'),
            'teacher_email' => $this->input->post('teacher_email'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('teachers',$data);
    }
    
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('teachers',$data);
    }

    public function get($id) {
        $this->db->select()->from('teachers');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    
    public function get_teacher_list(){
        $this->db->select()->from('teachers');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function teacherList($limit, $start){
        
        $data = array();        
        $this->db->limit($limit, $start);
        $this->db->select('teachers.*')->from('teachers');   
        $this->db->where('active', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result_array();
        } else {
            $data['data'] = array();
        }
        
        $this->db->select('count(*) as ncount');
        $this->db->from('teachers');   
        $this->db->where('active', 1);
        $query = $this->db->get();
        $count = 0;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $count = $row['ncount'];
        }
        $data['recordsFiltered']= $count;
        $data['recordsTotal'] = $count;        
        return $data;
    }
}
