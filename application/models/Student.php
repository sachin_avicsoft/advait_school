<?php

class Student extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function add(){
        $data = array(
            'student_name' => $this->input->post('student_name'),
            'student_address' => $this->input->post('student_address'),
            'student_contact' => $this->input->post('student_contact'),
            'student_gender' => $this->input->post('student_gender'),
            'student_birth_date' => $this->input->post('student_birth_date'),
            'student_email' => $this->input->post('student_email'),
            'batch_id' => $this->input->post('batch_id'),
            'createdAt' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('students',$data);
        return $this->db->insert_id();
    }
    
    public function update(){
        $data = array(
            'student_name' => $this->input->post('student_name'),
            'student_address' => $this->input->post('student_address'),
            'student_contact' => $this->input->post('student_contact'),
            'student_gender' => $this->input->post('student_gender'),
            'student_birth_date' => $this->input->post('student_birth_date'),
            'student_email' => $this->input->post('student_email'),
            'batch_id' => $this->input->post('batch_id'),
            'updatedAt' => date('Y-m-d H:i:s'),
        );
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('students',$data);
    }
    
    public function delete(){
        $data = array(
            'active' => 0,
        );
        $this->db->where('id',$this->input->get('id'));
        $this->db->update('students',$data);
    }

    public function get($id) {
        $this->db->select()->from('students');
        $this->db->where('id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return FALSE;
    }
    
    public function get_student_list(){
        $this->db->select()->from('students');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function get_student_count_by_batch($id,$gender){
        $this->db->select('count(*) as ncount')->from('students');
        $this->db->where('student_gender', $gender);
        $this->db->where('batch_id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function get_student_list_by_batch($id){
        $this->db->select()->from('students');
        $this->db->where('batch_id', $id);
        $this->db->where('active', 1);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        }
        return FALSE;
    }

    public function studentList($limit, $start, $search){
        
        $data = array();        
        $this->db->limit($limit, $start);
        $this->db->select('students.*,teachers.teacher_name,standards.standard_name,batches.batch_name')->from('students');   
        $this->db->join('batches','students.batch_id = batches.id','left outer');
        $this->db->join('teachers','batches.teacher_id = teachers.id','left outer');
        $this->db->join('standards','batches.standard_id = standards.id','left outer');
        $this->db->where('students.active', 1);
        if($search != ""){
            $where = "students.student_name LIKE '%$search%'";
            $this->db->where($where);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data['data'] = $query->result_array();
        } else {
            $data['data'] = array();
        }
        
        $this->db->select('count(*) as ncount');
        $this->db->from('students');   
        $this->db->join('batches','students.batch_id = batches.id','left outer');
        $this->db->join('teachers','batches.teacher_id = teachers.id','left outer');
        $this->db->join('standards','batches.standard_id = standards.id','left outer');
        $this->db->where('students.active', 1);
        if($search != ""){
            $where = "students.student_name LIKE '%$search%'";
            $this->db->where($where);
        }
        $query = $this->db->get();
        $count = 0;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $count = $row['ncount'];
        }
        $data['recordsFiltered']= $count;
        $data['recordsTotal'] = $count;        
        return $data;
    }
}
